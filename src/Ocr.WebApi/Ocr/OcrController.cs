﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ocr.WebApi.Ocr
{
    public class OcrController : ControllerBase
    {
        private readonly OcrSocketConnectionHandler _ocrSocketConnectionHandler;

        public OcrController(
            OcrSocketConnectionHandler ocrSocketConnectionHandler)
        {
            _ocrSocketConnectionHandler = ocrSocketConnectionHandler;
        }

        [Authorize]
        [Route("api/ocr")]
        public async Task Recognize()
        {
            await _ocrSocketConnectionHandler.HandleSocketConnection(ControllerContext.HttpContext);
        }
    }
}
