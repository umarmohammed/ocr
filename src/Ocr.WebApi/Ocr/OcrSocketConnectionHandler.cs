﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.ML;
using Ocr.WebApi.Actions;
using Ocr.WebApi.Extensions;
using Ocr.WebApi.ML;
using Ocr.WebApi.WebSockets;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace Ocr.WebApi.Ocr
{
    public class OcrSocketConnectionHandler : SocketHandler
    {
        private readonly WebSocketConnectionManager _webSocketConnectionManager;
        private readonly PredictionEnginePool<MLInput, MLOutput> _predictionEngine;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OcrSocketConnectionHandler(
            WebSocketConnectionManager webSocketConnectionManager, 
            PredictionEnginePool<MLInput, MLOutput> predictionEngine,
            IHttpContextAccessor httpContextAccessor)
            : base("jwt")
        {
            _webSocketConnectionManager = webSocketConnectionManager;
            _predictionEngine = predictionEngine;
            _httpContextAccessor = httpContextAccessor;
        }

        public override async Task OnConnected(WebSocket socket)
        {
            var userConnected = UserAction.UserConnected(_httpContextAccessor.HttpContext.GetUserName());
            await SendMessageToViewer(userConnected);
        }

        public override async Task OnMessage(WebSocket socket, WebSocketData webSocketData)
        {
            switch(webSocketData.Result.MessageType)
            {
                case WebSocketMessageType.Text:
                    await HandleSocketTextMessage(socket, webSocketData);
                    break;
                case WebSocketMessageType.Close:
                    await HandleSocketCloseMessage();
                    break;
                default:
                    break;
            }
        }

        private async Task HandleSocketTextMessage(WebSocket socket, WebSocketData webSocketData)
        {
            var action = UserActionFactory.WebSocketDataToAction(webSocketData);

            if (action is UserComputeOcr userComputeOcr)
            {
                await HandleComputeOcrMessage(socket, userComputeOcr);
                return;
            }
                
            await SendMessageToViewer(action);
        }

        private async Task HandleSocketCloseMessage()
        {
            var userDisconnected = UserAction.UserDisconnected(_httpContextAccessor.HttpContext.GetUserName());
            await SendMessageToViewer(userDisconnected);
        }

        private async Task HandleComputeOcrMessage(WebSocket socket, UserComputeOcr userComputeOcr)
        {
            var input = new MLInput() { PixelValues = userComputeOcr.Payload.Image };
            var prediction = _predictionEngine.Predict(input);

            var resultAction = new UserOcrComputed
            {
                Payload = new UserOcrComputedPayload
                {
                    Name = userComputeOcr.Payload.Name,
                    OcrResult = prediction.Score.IndexOfMax()
                }
            };

            await SendMessageToViewer(resultAction);
            await SendMessageAsync(socket, resultAction);
        }

        private async Task SendMessageToViewer(IUserAction action)
        {
            foreach (var socket in _webSocketConnectionManager.GetViewerSockets())
            {
                await SendMessageAsync(socket, action);
            }
        }
    }
}
