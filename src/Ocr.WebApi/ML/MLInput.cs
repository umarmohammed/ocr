﻿using Microsoft.ML.Data;

namespace Ocr.WebApi.ML
{
    public class MLInput
    {
        [ColumnName("PixelValues")]
        [VectorType(64)]
        public float[] PixelValues;

        [LoadColumn(64)]
        public float Number;
    }
}
