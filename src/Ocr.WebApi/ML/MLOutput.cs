﻿using Microsoft.ML.Data;

namespace Ocr.WebApi.ML
{
    public class MLOutput
    {
        [ColumnName("Score")]
        public float[] Score;
    }
}
