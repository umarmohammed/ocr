﻿using Microsoft.AspNetCore.Http;
using Ocr.WebApi.Actions;
using Ocr.WebApi.Extensions;
using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ocr.WebApi.WebSockets
{
    public abstract class SocketHandler
    {
        private readonly string _subProtocol;

        public SocketHandler(string subProtocol = null)
        {
            _subProtocol = subProtocol;
        }

        public async Task HandleSocketConnection(HttpContext context)
        {
            if (context.WebSockets.IsWebSocketRequest)
            {
                var socket = await GetWebSocket(context);
                await OnConnected(socket);

                await Receive(socket, async (socket, data) => await HandleMessage(socket, data));
            }
            else
            {
                context.Response.StatusCode = 400;
            }
        }

        public virtual async Task OnConnected(WebSocket socket)
        {
        }

        public virtual async Task OnMessage(WebSocket socket, WebSocketData webSocketData)
        {
        }

        protected async Task SendMessageAsync(WebSocket socket, IUserAction action)
        {
            if (socket.State != WebSocketState.Open)
                return;

            var message = JsonExtensions.SerializeObjectCamelCase(action);

            await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.ASCII.GetBytes(message),
                                                                  offset: 0,
                                                                  count: message.Length),
                                   messageType: WebSocketMessageType.Text,
                                   endOfMessage: true,
                                   cancellationToken: CancellationToken.None);
        }

        private async Task<WebSocket> GetWebSocket(HttpContext context)
        {
            return string.IsNullOrEmpty(_subProtocol)
                ? await context.WebSockets.AcceptWebSocketAsync()
                : await context.WebSockets.AcceptWebSocketAsync(_subProtocol);
        }

        private async Task HandleMessage(WebSocket socket, WebSocketData webSocketData)
        {
            await OnMessage(socket, webSocketData);

            if (webSocketData.IsClose)
            {
                await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                                statusDescription: "Closed by the WebSocketManager",
                                cancellationToken: CancellationToken.None);
                return;
            }
        }

        private async Task Receive(WebSocket socket, Action<WebSocket, WebSocketData> handleMessage)
        {
            var buffer = new byte[1024 * 4];

            while (socket.State == WebSocketState.Open)
            {
                var result = await socket.ReceiveAsync(buffer: new ArraySegment<byte>(buffer),
                                                       cancellationToken: CancellationToken.None);

                handleMessage(socket, new WebSocketData { Buffer = buffer, Result = result });
            }
        }
    }
}
