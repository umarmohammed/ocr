﻿using System.Net.WebSockets;
using System.Text;

namespace Ocr.WebApi.WebSockets
{
    public class WebSocketData
    {
        public byte[] Buffer { get; set; }
        public WebSocketReceiveResult Result { get; set; }

        public bool IsClose => Result.MessageType == WebSocketMessageType.Close;
        public override string ToString()
        {
            return Encoding.UTF8.GetString(Buffer, 0, Result.Count);
        }
    }
}
