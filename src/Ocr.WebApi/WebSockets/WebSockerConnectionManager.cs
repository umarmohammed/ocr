﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.WebSockets;

namespace Ocr.WebApi.WebSockets
{
    public class WebSocketConnectionManager
    {
        private ConcurrentDictionary<WebSocket, int> _viewerSockets = new ConcurrentDictionary<WebSocket, int>();

        public ICollection<WebSocket> GetViewerSockets()
        {
            return _viewerSockets.Keys;
        }

        public void AddViewerSocket(WebSocket socket)
        {
            _viewerSockets.TryAdd(socket, 0);
        }

        public void RemoveViewerSocket(WebSocket socket)
        {
            _viewerSockets.TryRemove(socket, out var _);
        }
    }
}
