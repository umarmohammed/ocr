﻿namespace Ocr.WebApi.Extensions
{
    public static class ArrayExtension
    {
        public static int IndexOfMax(this float[] array)
        {
            var max = array[0];
            var maxIndex = 0;
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    maxIndex = i;
                    max = array[i];
                }
            }

            return maxIndex;
        }
    }
}
