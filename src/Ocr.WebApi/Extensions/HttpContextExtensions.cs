﻿using Microsoft.AspNetCore.Http;

namespace Ocr.WebApi.Extensions
{
    public static class HttpContextExtensions
    {
        public static string GetUserName(this HttpContext httpContext)
        {
            return httpContext.User.FindFirst("name").Value;
        } 
    }
}
