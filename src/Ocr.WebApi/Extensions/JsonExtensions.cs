﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ocr.WebApi.Extensions
{
    public static class JsonExtensions
    {
        private static JsonSerializerSettings JsonCamelCaseSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public static string SerializeObjectCamelCase(object value)
        {
            return JsonConvert.SerializeObject(value, JsonCamelCaseSerializerSettings);
        }
    }
}
