﻿using Ocr.WebApi.WebSockets;

namespace Ocr.WebApi.Actions
{
    public static class UserActionFactory
    {
        public const string USER_CONNECTED = "User Connected";
        public const string USER_DISCONNECTED = "User Disconnected";
        public const string USER_COMPUTE_OCR = "User Compute OCR";
        public const string OCR_COMPUTED = "OCR Computed";

        public static IUserAction WebSocketDataToAction(WebSocketData data)
        {
            var action = UserAction.FromWebSocketData(data);

            switch (action.Type)
            {
                case USER_COMPUTE_OCR:
                {
                   return UserComputeOcr.FromUserAction(action);
                } 
                default:
                    return action;
            }
        }
    }
}
