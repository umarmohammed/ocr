﻿namespace Ocr.WebApi.Actions
{
    public class UserOcrComputed : IUserAction
    {
        public string Type => UserActionFactory.OCR_COMPUTED;
        public UserOcrComputedPayload Payload { get; set; }
    }

    public class UserOcrComputedPayload
    {
        public string Name { get; set; }
        public int OcrResult { get; set; }
    }
}
