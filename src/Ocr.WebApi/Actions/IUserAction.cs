﻿namespace Ocr.WebApi.Actions
{
    public interface IUserAction
    {
        string Type { get; }
    }
}
