﻿using Newtonsoft.Json;

namespace Ocr.WebApi.Actions
{
    public class UserComputeOcr : IUserAction
    {
        public string Type => UserActionFactory.USER_COMPUTE_OCR;
        public UserComputeOcrPayload Payload { get; set; }

        public static UserComputeOcr FromUserAction(UserAction action)
        {
            return new UserComputeOcr
            {
                Payload = JsonConvert.DeserializeObject<UserComputeOcrPayload>(action.Payload.ToString())
            };
        }
    }

    public class UserComputeOcrPayload
    {
        public string Name { get; set; }
        public float[] Image { get; set; }
    }
}
