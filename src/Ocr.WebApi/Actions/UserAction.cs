﻿using Newtonsoft.Json;
using Ocr.WebApi.WebSockets;

namespace Ocr.WebApi.Actions
{
    public class UserAction : IUserAction
    {
        public string Type { get; set; }
        public object Payload { get; set; }

        public static UserAction FromWebSocketData(WebSocketData data)
        {
            return JsonConvert.DeserializeObject<UserAction>(data.ToString());
        }

        public static UserAction UserConnected(string user) => 
            new UserAction 
            { 
                Type = UserActionFactory.USER_CONNECTED,
                Payload = user 
            };

        public static UserAction UserDisconnected(string user) => 
            new UserAction 
            {
                Type = UserActionFactory.USER_DISCONNECTED, 
                Payload = user 
            };
    }
}
