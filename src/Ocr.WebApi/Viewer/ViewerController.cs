﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ocr.WebApi.Viewer
{
    public class ViewerController : ControllerBase
    {
        private readonly ViewerSocketConnectionHandler _handler;

        public ViewerController(ViewerSocketConnectionHandler handler)
        {
            _handler = handler;
        }

        [Route("api/viewer")]
        public async Task Users()
        {
            await _handler.HandleSocketConnection(ControllerContext.HttpContext);
        }
    }
}
