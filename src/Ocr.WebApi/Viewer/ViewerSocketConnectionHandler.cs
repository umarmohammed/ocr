﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using Ocr.WebApi.WebSockets;

namespace Ocr.WebApi.Viewer
{
    public class ViewerSocketConnectionHandler : SocketHandler
    {
        private readonly WebSocketConnectionManager _webSocketConnectionManager;

        public ViewerSocketConnectionHandler(WebSocketConnectionManager webSocketConnectionManager) 
        {
            _webSocketConnectionManager = webSocketConnectionManager;
        }

        public override async Task OnConnected(WebSocket socket)
        {
            _webSocketConnectionManager.AddViewerSocket(socket);
            await Task.FromResult<object>(null);
        }

        public override async Task OnMessage(WebSocket socket, WebSocketData webSocketData)
        {
            if (webSocketData.IsClose)
            {
                _webSocketConnectionManager.RemoveViewerSocket(socket);
            }
        }
    }
}
