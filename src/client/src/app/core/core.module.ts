import { NgModule } from '@angular/core';
import { CanvasService } from './canvas.service';

@NgModule({
  providers: [CanvasService]
})
export class CoreModule {}
