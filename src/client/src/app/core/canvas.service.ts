import { Injectable, ElementRef } from '@angular/core';
import { Point } from 'src/app/shared/point';

@Injectable({ providedIn: 'root' })
export class CanvasService {
  initCtx(canvas: ElementRef): CanvasRenderingContext2D {
    const ctx = canvas.nativeElement.getContext('2d');
    ctx.lineWidth = 35;
    ctx.lineCap = 'round';
    return ctx;
  }

  drawOcrResult(ctx: CanvasRenderingContext2D, ocrResult: string) {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    // hard coded for 300x300
    ctx.font = '350px Arial';
    ctx.fillText(ocrResult, 50, 280);
  }

  copyCanvasImage(
    srcCtx: CanvasRenderingContext2D,
    targetCtx: CanvasRenderingContext2D
  ) {
    const [targetCtxWidth, targetCtxHeight] = [
      targetCtx.canvas.width,
      targetCtx.canvas.height
    ];

    targetCtx.clearRect(0, 0, targetCtxWidth, targetCtxHeight);
    targetCtx.drawImage(srcCtx.canvas, 0, 0, targetCtxWidth, targetCtxHeight);
  }

  resetCanvas(ctx: CanvasRenderingContext2D) {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }

  drawPoint(ctx: CanvasRenderingContext2D, point: Point) {
    point.start
      ? this.drawStartPoint(ctx, point)
      : this.drawMovePoint(ctx, point);
  }

  private drawStartPoint(ctx: CanvasRenderingContext2D, point: Point) {
    ctx.beginPath();
    ctx.moveTo(point.x, point.y);
  }

  private drawMovePoint(ctx: CanvasRenderingContext2D, point: Point) {
    ctx.lineTo(point.x, point.y);
    ctx.stroke();
    ctx.moveTo(point.x, point.y);
  }
}
