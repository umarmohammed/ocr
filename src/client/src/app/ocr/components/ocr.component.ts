import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-ocr',
  template: `
    <app-canvas></app-canvas>
    <app-result></app-result>
    <img src="assets/clouds.svg" />
  `,
  styles: [
    `
      :host {
        display: grid;
        place-items: center;
        height: 80vh;
        font-family: var(--main-font-family);
      }

      img {
        position: fixed;
        bottom: -160px;
        left: -230px;
        z-index: -10;
        width: 1920px;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OcrComponent {}
