import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-digit-box',
  template: `
    <img class="container" [src]="src || ''" />
    <p class="center-text">{{ text }}</p>
  `,
  styles: [
    `
      img {
        border: 2px solid #1976d2;
        width: 100px;
        height: 100px;
      }

      p {
        margin: 0;
      }
    `
  ]
})
export class DigitBoxComponent {
  @Input()
  src: string;

  @Input()
  text: string;
}
