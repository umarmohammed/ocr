import { Component } from '@angular/core';
import { OcrService } from '../services/ocr.service';
import { OcrCanvasService } from '../services/ocr-canvas.service';

@Component({
  selector: 'app-result',
  template: `
    <app-digit-box
      text="you drew"
      [src]="ocrCanvas.characterImageDataUrl$ | async"
    ></app-digit-box>
    <app-digit-box text="result" [src]="ocr.result$ | async"> </app-digit-box>
  `,
  styles: [
    `
      :host {
        display: grid;
        grid-template-columns: auto auto;
        grid-column-gap: 20px;
      }
    `
  ]
})
export class ResultComponent {
  constructor(public ocr: OcrService, public ocrCanvas: OcrCanvasService) {}
}
