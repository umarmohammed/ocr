import {
  Component,
  AfterViewInit,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy
} from '@angular/core';
import { OcrCanvasService } from '../services/ocr-canvas.service';

@Component({
  selector: 'app-canvas',
  template: `
    <canvas
      #canvas
      width="300"
      height="300"
      (touchmove)="ocr.onTouch($event)"
      (touchstart)="ocr.onTouch($event)"
    ></canvas>

    <p class="center-text">Draw a number in the box above</p>
  `,
  styles: [
    `
      canvas {
        border: 2px solid #1976d2;
        border-radius: 5px;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CanvasComponent implements AfterViewInit {
  @ViewChild('canvas', { static: false }) canvas: ElementRef;

  constructor(public ocr: OcrCanvasService) {}

  ngAfterViewInit() {
    this.ocr.initCanvas(this.canvas);
  }
}
