import { Point } from 'src/app/shared/point';
import { ind2subs } from './constants';

export function touchEventToPoint(
  ctx: CanvasRenderingContext2D,
  touchEvent: TouchEvent
): Point {
  return {
    x: touchEvent.touches[0].clientX - ctx.canvas.offsetLeft,
    y: touchEvent.touches[0].clientY - ctx.canvas.offsetTop,
    start: touchEvent.type === 'touchstart'
  };
}

export function canvasElToMnistEncodedArray(canvasEl: HTMLCanvasElement) {
  const resizedImageData = getResizedImageData(canvasEl);
  const grayscale = getGrayscalePixels(resizedImageData);
  return getMnistProcessedArray(grayscale);
}

// Hard coded for 32x32
export function getResizedImageData(canvasEl: HTMLCanvasElement) {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');

  const width = 32;
  const height = 32;

  canvas.width = width;
  canvas.height = height;

  ctx.drawImage(canvasEl, 0, 0, width, height);
  return ctx.getImageData(0, 0, width, height);
}

// Hard coded for 32x32
export function getGrayscalePixels(imageData: ImageData) {
  const data = imageData.data;
  const grayscale = new Array(32 * 32);

  let grayscaleIndex = 0;
  for (let i = 0; i < data.length; i += 4) {
    const pixelVal = data[i + 3];
    grayscale[grayscaleIndex] = pixelVal;
    grayscaleIndex++;
  }

  return grayscale;
}

// hard coded for 32x32 images
export function getMnistProcessedArray(grayscaleImage: number[]) {
  const arr = new Array(64);
  for (let i = 0; i < 64; i++) {
    arr[i] = countNonZeros(grayscaleImage, i);
  }

  return arr;
}

export function countNonZeros(grayscale: number[], index: number) {
  const imageIndexes = ind2subs[index];

  const block = imageIndexes.map(imageIndex => grayscale[imageIndex]);

  return block.reduce(
    (count, pixelValue) => (pixelValue > 0 ? count + 1 : count),
    0
  );
}

export function numberToImage(value: number, width, height) {
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');

  canvas.width = width;
  canvas.height = height;

  ctx.font = '100px Arial';
  ctx.fillText(value.toString(), 20, 90);
  return canvas.toDataURL();
}
