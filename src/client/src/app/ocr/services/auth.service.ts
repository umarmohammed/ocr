import { Injectable } from '@angular/core';
import { MsalService } from '@azure/msal-angular';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private msal: MsalService) {}

  // TODO: A hack
  get token(): string {
    return sessionStorage.getItem('msal.idtoken');
  }

  get name(): string {
    return this.msal.getUser().name;
  }
}
