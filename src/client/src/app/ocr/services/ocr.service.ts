import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { webSocket } from 'rxjs/webSocket';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import {
  UserMouseMove,
  UserComputeOcr,
  OcrComputed
} from 'src/app/shared/actions';
import { Point } from 'src/app/shared/point';
import {
  numberToImage,
  canvasElToMnistEncodedArray
} from '../util/canvas-utils';
import { OcrCanvasService } from './ocr-canvas.service';

@Injectable({ providedIn: 'root' })
export class OcrService {
  private url = `${environment.baseUrl}api/ocr`;

  private subject = webSocket({
    url: this.url,
    protocol: ['jwt', this.auth.token]
  });

  result$ = this.subject.pipe(
    map((action: OcrComputed) =>
      numberToImage(action.payload.ocrResult, 100, 100)
    )
  );

  constructor(private auth: AuthService, private canvas: OcrCanvasService) {
    this.canvas.point$.subscribe(point => this.newPoint(point));
    this.canvas.characterImage$.subscribe(canvasEl =>
      this.characterDrawn(canvasEl)
    );
  }

  newPoint(point: Point) {
    this.dispatch(new UserMouseMove({ point, name: this.auth.name }));
  }

  characterDrawn(canvasEl: HTMLCanvasElement) {
    this.dispatch(
      new UserComputeOcr({
        image: canvasElToMnistEncodedArray(canvasEl),
        name: this.auth.name
      })
    );
  }

  private dispatch(action: UserMouseMove | UserComputeOcr) {
    this.subject.next(action);
  }
}
