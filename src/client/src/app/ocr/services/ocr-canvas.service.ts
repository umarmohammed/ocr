import { Injectable, ElementRef } from '@angular/core';
import { Point } from 'src/app/shared/point';
import { touchEventToPoint } from '../util/canvas-utils';
import { Subject } from 'rxjs';
import { debounceTime, map, tap } from 'rxjs/operators';
import { CanvasService } from 'src/app/core/canvas.service';

@Injectable({ providedIn: 'root' })
export class OcrCanvasService {
  private ctx: CanvasRenderingContext2D;

  private pointSubject = new Subject<Point>();
  point$ = this.pointSubject.asObservable();

  characterImage$ = this.pointSubject.pipe(
    debounceTime(1000),
    map(() => this.ctx.canvas)
  );

  characterImageDataUrl$ = this.characterImage$.pipe(
    map(canvasEl => canvasEl.toDataURL()),
    tap(() => this.clear())
  );

  constructor(private canvasService: CanvasService) {}

  initCanvas(canvas: ElementRef) {
    this.ctx = this.canvasService.initCtx(canvas);
  }

  onTouch(touchEvent: TouchEvent) {
    const point = touchEventToPoint(this.ctx, touchEvent);
    this.canvasService.drawPoint(this.ctx, point);
    this.pointSubject.next(point);
  }

  private clear() {
    this.canvasService.resetCanvas(this.ctx);
  }
}
