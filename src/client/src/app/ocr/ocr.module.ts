import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { OcrComponent } from './components/ocr.component';
import { CanvasComponent } from './components/canvas.component';
import { ResultComponent } from './components/result.component';
import { DigitBoxComponent } from './components/digit-box.component';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MsalGuard } from '@azure/msal-angular';

const routes: Routes = [
  { path: '', component: OcrComponent, canActivate: [MsalGuard] }
];

@NgModule({
  declarations: [
    OcrComponent,
    CanvasComponent,
    ResultComponent,
    DigitBoxComponent
  ],
  imports: [CommonModule, HttpClientModule, RouterModule.forChild(routes)]
})
export class OcrModule {}
