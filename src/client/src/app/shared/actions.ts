import { Point } from 'src/app/shared/point';

export const USER_CONNECTED = 'User Connected';
export const USER_DISCONNECTED = 'User Disconnected';
export const USER_MOUSE_MOVE = 'User Mouse Move';
export const USER_COMPUTE_OCR = 'User Compute OCR';
export const OCR_COMPUTED = 'OCR Computed';

export interface Action {
  type: string;
  payload: any;
}

export class UserConnected implements Action {
  readonly type = USER_CONNECTED;
  constructor(public payload: string) {}
}

export class UserDisconnected implements Action {
  readonly type = USER_DISCONNECTED;
  constructor(public payload: string) {}
}

export class UserMouseMove implements Action {
  readonly type = USER_MOUSE_MOVE;
  constructor(public payload: { name: string; point: Point }) {}
}

export class OcrComputed implements Action {
  readonly type = OCR_COMPUTED;
  constructor(public payload: { name: string; ocrResult: number }) {}
}

export class UserComputeOcr {
  readonly type = USER_COMPUTE_OCR;
  constructor(public payload: { name: string; image: number[] }) {}
}

export type Actions =
  | UserConnected
  | UserDisconnected
  | UserMouseMove
  | OcrComputed;
