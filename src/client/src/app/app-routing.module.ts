import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'ocr',
    loadChildren: () => import('../app/ocr/ocr.module').then(m => m.OcrModule)
  },
  {
    path: 'viewer',
    loadChildren: () =>
      import('../app/viewer/viewer.module').then(m => m.ViewerModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
