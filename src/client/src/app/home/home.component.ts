import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <div>
      <a class="btn" [routerLink]="['/ocr']">Ocr</a>
      <a class="btn ml-1" [routerLink]="['/viewer']">Viewer</a>
    </div>
  `,
  styles: [
    `
      :host {
        display: grid;
        height: 80vh;
        place-items: center;
      }
      a {
        font-family: var(--main-font-family);
      }
    `
  ]
})
export class HomeComponent {}
