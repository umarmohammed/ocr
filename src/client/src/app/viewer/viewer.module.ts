import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { ViewerComponent } from './viewer.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [{ path: '', component: ViewerComponent }];

@NgModule({
  declarations: [ViewerComponent, UserComponent],
  imports: [CommonModule, RouterModule.forChild(routes)]
})
export class ViewerModule {}
