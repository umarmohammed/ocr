import { State, initialUserState } from './state';
import * as fromActions from '../../shared/actions';

export function reducer(state: State, action: fromActions.Actions) {
  switch (action.type) {
    case fromActions.USER_CONNECTED: {
      const user = initialUserState(action.payload);
      const users = [...state.users, user];

      return {
        ...state,
        users
      };
    }

    case fromActions.USER_DISCONNECTED: {
      const users = state.users.filter(user => user.name !== action.payload);
      return {
        ...state,
        users
      };
    }

    case fromActions.USER_MOUSE_MOVE: {
      const users = state.users.map(user =>
        user.name === action.payload.name
          ? {
              ...user,
              drawnPoint: action.payload.point,
              ocrResult: null,
              drawing: true
            }
          : user
      );

      return {
        ...state,
        users
      };
    }

    case fromActions.OCR_COMPUTED: {
      const users = state.users.map(user =>
        user.name === action.payload.name
          ? { ...user, ocrResult: action.payload.ocrResult, drawing: false }
          : user
      );

      return {
        ...state,
        users
      };
    }
  }
  return state;
}
