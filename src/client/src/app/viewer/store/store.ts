import { BehaviorSubject } from 'rxjs';
import { State, initialState } from './state';
import { Actions } from '../../shared/actions';
import { reducer } from './reducer';
import { Injectable } from '@angular/core';
import { map, tap, switchMap } from 'rxjs/operators';
import { ViewerService } from '../viewer.service';

@Injectable({ providedIn: 'root' })
export class Store {
  private state = new BehaviorSubject<State>(initialState);

  constructor(private viewerService: ViewerService) {}

  users$ = this.viewerService.userAction$.pipe(
    tap((action: Actions) => this.reduce(action)),
    switchMap(() => this.state.pipe(map(s => s.users)))
  );

  private reduce(action: Actions) {
    this.state.next(reducer(this.state.value, action));
  }
}
