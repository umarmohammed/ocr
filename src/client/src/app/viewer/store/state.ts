import { Point } from 'src/app/shared/point';

export interface UserState {
  name: string;
  drawnPoint: Point;
  ocrResult: number;
  drawing: boolean;
}

export const initialUserState = (user: string): UserState => ({
  name: user,
  drawnPoint: null,
  ocrResult: null,
  drawing: false
});

export interface State {
  users: UserState[];
}

export const initialState: State = {
  users: []
};
