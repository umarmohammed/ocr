import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from './store/store';
import { UserState } from './store/state';

@Component({
  selector: 'app-viewer',
  template: `
    <app-user
      *ngFor="let user of users$ | async; trackBy: trackByFn"
      [user]="user"
    ></app-user>
  `,
  styles: [
    `
      :host {
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(302px, 1fr));
        grid-gap: 10px;
        grid-auto-rows: minmax(100px, auto);
        font-family: var(--main-font-family);
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewerComponent {
  users$ = this.store.users$;

  constructor(private store: Store) {}

  trackByFn(_: number, item: UserState) {
    return item.name;
  }
}
