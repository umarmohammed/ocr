import { Injectable } from '@angular/core';
import { webSocket } from 'rxjs/webSocket';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ViewerService {
  userAction$ = webSocket({
    url: `${environment.baseUrl}api/viewer`
  }).asObservable();
}
