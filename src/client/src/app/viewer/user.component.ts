import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { UserState } from './store/state';
import { CanvasService } from '../core/canvas.service';

@Component({
  selector: 'app-user',
  template: `
    <div class="card">
      <p class="center-text">{{ user.name }}</p>
      <div class="progress-line" [class.show]="user.drawing"></div>
      <canvas
        class="small-canvas"
        [class.show]="!user.drawing"
        #smallCanvas
        width="70"
        height="70"
      ></canvas>
      <canvas #canvas width="300" height="300"></canvas>
    </div>
  `,
  styles: [
    `
      canvas {
        background: #f8f9fa;
      }
      .small-canvas {
        position: absolute;
        visibility: hidden;
        background: #fefeee;
        top: 300px;
      }
      .small-canvas.show {
        visibility: visible;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserComponent implements AfterViewInit, OnChanges {
  @Input() user: UserState;
  @ViewChild('canvas', { static: false }) canvas: ElementRef;
  @ViewChild('smallCanvas', { static: false }) smallCanvas: ElementRef;

  constructor(private canvasService: CanvasService) {}

  private ctx: CanvasRenderingContext2D;
  private smallCanvasCtx: CanvasRenderingContext2D;

  ngAfterViewInit(): void {
    this.ctx = this.canvasService.initCtx(this.canvas);
    this.smallCanvasCtx = this.smallCanvas.nativeElement.getContext('2d');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.ctx) {
      return;
    }

    this.user.ocrResult !== null
      ? this.handleOcrEvent()
      : this.handleDrawEvent(changes);
  }

  private handleOcrEvent() {
    this.canvasService.copyCanvasImage(this.ctx, this.smallCanvasCtx);
    this.canvasService.drawOcrResult(this.ctx, this.user.ocrResult.toString());
  }

  private handleDrawEvent(changes: SimpleChanges) {
    // ugly hack to clear canvas if previous was an ocr event
    // should tidy up but cba
    const previousOcr: number =
      changes.user.previousValue && changes.user.previousValue.ocrResult;
    if (previousOcr !== null) {
      this.canvasService.resetCanvas(this.ctx);
    }
    this.canvasService.drawPoint(this.ctx, this.user.drawnPoint);
  }
}
