import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { MsalModule } from '@azure/msal-angular';
import { HttpClientModule } from '@angular/common/http';
import { HomeModule } from './home/home.module';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule,
    AppRoutingModule,
    HttpClientModule,
    HomeModule,
    MsalModule.forRoot({
      clientID: 'e9edb7e2-4393-4328-8ad2-8794309bd2e1',
      redirectUri: environment.redirectUri
    })
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
