export const environment = {
  production: true,
  baseUrl: 'wss://ocr-demo.azurewebsites.net/',
  redirectUri: 'https://ocrd.azurewebsites.net/ocr' // hack
};
